package foodstarEJB.model.managers;

import java.util.ArrayList;
import java.util.List;

import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import foodstarEJB.model.entities.Direccion;

/**
 * Session Bean implementation class ManagerDireccion
 */
@Stateless
@LocalBean
public class ManagerDireccion {
	// Atributos
	@PersistenceContext
	private EntityManager em;
	

    /**
     * Default constructor. 
     */
    public ManagerDireccion() {
        // TODO Auto-generated constructor stub
    }

    public List<Direccion> findAllDireccion() {
    	return em.createNamedQuery("Direccion.findAll", Direccion.class).getResultList();
    }
    
    public void registrarDireccion(String callePrincipal, String calleSecundaria) throws Exception {
    	if(callePrincipal.isEmpty()) {
    		throw new Exception("Debe ingresar una calle principal");
    	}
    	if (calleSecundaria.isEmpty()) {
			throw new Exception("Debe ingresar una calle secundaria");
		}
    	Direccion d;
    	d = new Direccion();
    	d.setCallePrincipal(callePrincipal);
    	d.setCalleSecundaria(calleSecundaria);
    	
    	try {
			em.persist(d);
		} catch (Exception e) {
			// TODO: handle exception
			throw new Exception("Error en el registro de direcci�n, esta direccion ya existe.");
		}
    }
}
