package foodstarEJB.model.entities;

import java.io.Serializable;
import javax.persistence.*;
import java.util.List;


/**
 * The persistent class for the persona database table.
 * 
 */
@Entity
@NamedQuery(name="Persona.findAll", query="SELECT p FROM Persona p")
public class Persona implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	private String username;

	private String apellido;

	private String contrasenia;

	@Column(name="correo_electronico")
	private String correoElectronico;

	private String nombre;

	//bi-directional many-to-one association to Estado
	@ManyToOne
	@JoinColumn(name="id_estado")
	private Estado estado;

	//bi-directional many-to-one association to TipoDePersona
	@ManyToOne
	@JoinColumn(name="id_tipo_p")
	private TipoDePersona tipoDePersona;

	//bi-directional many-to-one association to Restaurante
	@OneToMany(mappedBy="persona")
	private List<Restaurante> restaurantes;

	//bi-directional many-to-one association to Calificacion
	@OneToMany(mappedBy="persona")
	private List<Calificacion> calificacions;

	public Persona() {
	}

	public String getUsername() {
		return this.username;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	public String getApellido() {
		return this.apellido;
	}

	public void setApellido(String apellido) {
		this.apellido = apellido;
	}

	public String getContrasenia() {
		return this.contrasenia;
	}

	public void setContrasenia(String contrasenia) {
		this.contrasenia = contrasenia;
	}

	public String getCorreoElectronico() {
		return this.correoElectronico;
	}

	public void setCorreoElectronico(String correoElectronico) {
		this.correoElectronico = correoElectronico;
	}

	public String getNombre() {
		return this.nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	public Estado getEstado() {
		return this.estado;
	}

	public void setEstado(Estado estado) {
		this.estado = estado;
	}

	public TipoDePersona getTipoDePersona() {
		return this.tipoDePersona;
	}

	public void setTipoDePersona(TipoDePersona tipoDePersona) {
		this.tipoDePersona = tipoDePersona;
	}

	public List<Restaurante> getRestaurantes() {
		return this.restaurantes;
	}

	public void setRestaurantes(List<Restaurante> restaurantes) {
		this.restaurantes = restaurantes;
	}

	public Restaurante addRestaurante(Restaurante restaurante) {
		getRestaurantes().add(restaurante);
		restaurante.setPersona(this);

		return restaurante;
	}

	public Restaurante removeRestaurante(Restaurante restaurante) {
		getRestaurantes().remove(restaurante);
		restaurante.setPersona(null);

		return restaurante;
	}

	public List<Calificacion> getCalificacions() {
		return this.calificacions;
	}

	public void setCalificacions(List<Calificacion> calificacions) {
		this.calificacions = calificacions;
	}

	public Calificacion addCalificacion(Calificacion calificacion) {
		getCalificacions().add(calificacion);
		calificacion.setPersona(this);

		return calificacion;
	}

	public Calificacion removeCalificacion(Calificacion calificacion) {
		getCalificacions().remove(calificacion);
		calificacion.setPersona(null);

		return calificacion;
	}

}