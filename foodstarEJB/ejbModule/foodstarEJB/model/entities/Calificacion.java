package foodstarEJB.model.entities;

import java.io.Serializable;
import javax.persistence.*;
import java.math.BigDecimal;
import java.util.Date;


/**
 * The persistent class for the calificacion database table.
 * 
 */
@Entity
@NamedQuery(name="Calificacion.findAll", query="SELECT c FROM Calificacion c")
public class Calificacion implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(name="id_calificacion")
	private Integer idCalificacion;

	@Column(name="calificacion_dada")
	private BigDecimal calificacionDada;

	@Temporal(TemporalType.DATE)
	@Column(name="ultima_fecha_de_calificacion")
	private Date ultimaFechaDeCalificacion;

	//bi-directional many-to-one association to Persona
	@ManyToOne
	@JoinColumn(name="username")
	private Persona persona;

	//bi-directional many-to-one association to Restaurante
	@ManyToOne
	@JoinColumn(name="id_establecimiento")
	private Restaurante restaurante;

	public Calificacion() {
	}

	public Integer getIdCalificacion() {
		return this.idCalificacion;
	}

	public void setIdCalificacion(Integer idCalificacion) {
		this.idCalificacion = idCalificacion;
	}

	public BigDecimal getCalificacionDada() {
		return this.calificacionDada;
	}

	public void setCalificacionDada(BigDecimal calificacionDada) {
		this.calificacionDada = calificacionDada;
	}

	public Date getUltimaFechaDeCalificacion() {
		return this.ultimaFechaDeCalificacion;
	}

	public void setUltimaFechaDeCalificacion(Date ultimaFechaDeCalificacion) {
		this.ultimaFechaDeCalificacion = ultimaFechaDeCalificacion;
	}

	public Persona getPersona() {
		return this.persona;
	}

	public void setPersona(Persona persona) {
		this.persona = persona;
	}

	public Restaurante getRestaurante() {
		return this.restaurante;
	}

	public void setRestaurante(Restaurante restaurante) {
		this.restaurante = restaurante;
	}

}