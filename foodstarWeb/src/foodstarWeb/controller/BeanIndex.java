package foodstarWeb.controller;

import java.io.Serializable;

import javax.ejb.EJB;
import javax.enterprise.context.SessionScoped;
import javax.inject.Named;

import foodstarEJB.model.managers.ManagerPersona;

@Named
@SessionScoped
public class BeanIndex implements Serializable {

	@EJB
	private ManagerPersona m;
	
	public BeanIndex() {
		// TODO Auto-generated constructor stub
	}

	public String actionIniciarSesion() {
		return "inicioDeSesion?faces-redirect=true";
	}
	
	public String actionRegistro() {
		return "registro?faces-redirect=true"; 
	}
}
