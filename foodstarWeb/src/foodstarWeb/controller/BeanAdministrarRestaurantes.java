package foodstarWeb.controller;

import java.io.Serializable;
import java.sql.Connection;
import java.sql.DriverManager;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.PostConstruct;
import javax.ejb.EJB;
import javax.enterprise.context.SessionScoped;
import javax.faces.context.FacesContext;
import javax.inject.Named;
import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletResponse;
import javax.swing.plaf.synth.SynthSeparatorUI;

import foodstarEJB.model.entities.Restaurante;
import foodstarEJB.model.managers.ManagerRestaurante;
import foodstarWeb.view.util.Notification;
import net.sf.jasperreports.engine.JasperExportManager;
import net.sf.jasperreports.engine.JasperFillManager;
import net.sf.jasperreports.engine.JasperPrint;

@Named
@SessionScoped
public class BeanAdministrarRestaurantes implements Serializable {
	// Atributos
	@EJB
	private ManagerRestaurante m;
	private List<Restaurante> restaurantes;

	public BeanAdministrarRestaurantes() {
		// TODO Auto-generated constructor stub
	}

	@PostConstruct
	public void init() {
		restaurantes = m.findAllRestaurantes();
	}

	public String actionRegresar() {
		return "indexAdministrador.xhtml?faces-redirect=true";
	}

	public void actionListenerImprimirReporte() {
		Map<String, Object> parametros;
		parametros = new HashMap<>();
		FacesContext context;
		context = FacesContext.getCurrentInstance();
		ServletContext servletContext;
		servletContext = (ServletContext) context.getExternalContext().getContext();
		String path;
		path = servletContext.getRealPath("administrador/restaurantes.jasper");
		System.out.println(path);
		HttpServletResponse response;
		response = (HttpServletResponse) context.getExternalContext().getResponse();
		response.addHeader("Content-disposition", "attachment;filename=restaurantes.pdf");
		response.setContentType("application/pdf");
		try {
			Class.forName("org.postgresql.Driver");
			Connection connection = null;
			connection = DriverManager.getConnection("jdbc:postgresql://localhost:5432/foodstar", "postgres", "admin");
			JasperPrint impresion = JasperFillManager.fillReport(path, parametros, connection);
			JasperExportManager.exportReportToPdfStream(impresion, response.getOutputStream());
			context.getApplication().getStateManager().saveView(context);
			System.out.println("reporte generado.");
			context.responseComplete();
		} catch (Exception e) {
			Notification.error(e.getMessage());
			e.printStackTrace();
		}
	}

	public void actionListenerRefrescar() {
		restaurantes = m.findAllRestaurantes();
	}

	public List<Restaurante> getRestaurantes() {
		return restaurantes;
	}

	public void setRestaurantes(List<Restaurante> restaurantes) {
		this.restaurantes = restaurantes;
	}
}
