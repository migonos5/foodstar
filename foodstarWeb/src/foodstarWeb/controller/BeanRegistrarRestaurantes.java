package foodstarWeb.controller;

import java.io.Serializable;
import java.util.Collection;
import java.util.Iterator;
import java.util.List;
import java.util.ListIterator;

import javax.annotation.PostConstruct;
import javax.ejb.EJB;
import javax.enterprise.context.SessionScoped;
import javax.inject.Inject;
import javax.inject.Named;

import foodstarEJB.model.entities.Direccion;
import foodstarEJB.model.managers.ManagerDireccion;
import foodstarEJB.model.managers.ManagerRestaurante;
import foodstarWeb.view.util.Notification;

@Named
@SessionScoped
public class BeanRegistrarRestaurantes implements Serializable {
	@EJB
	private ManagerRestaurante mRestaurante;
	@EJB
	private ManagerDireccion mDireccion;
	@Inject
	private BeanInicioDeSesion b;
	private String callePrincipal;
	private String nuevaCallePrincipal;
	private String nuevaCalleSecundaria;
	private String nombre;
	private String descripcion;
	private String imagenInicial;
	private String descripcionImagen;
	private List<Direccion> direcciones;

	public BeanRegistrarRestaurantes() {
		// TODO Auto-generated constructor stub
	}

	@PostConstruct
	public void refrescar() {
		direcciones = mDireccion.findAllDireccion();
	}

	public void limpiarCampos() {
		nuevaCallePrincipal = "";
		nuevaCalleSecundaria = "";
		nombre = "";
		descripcion = "";
		imagenInicial = "";
		descripcionImagen = "";
	}

	public void actionListenerRegistrarDireccion() {
		try {
			mDireccion.registrarDireccion(nuevaCallePrincipal, nuevaCalleSecundaria);
			Notification.info("Direcci�n registrada correctamente");
			refrescar();
			limpiarCampos();
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			Notification.warning(e.getMessage());
		}
	}

	public void actionListenerRegistrarRestaurante() {
		try {
			System.out.println(b.getNombreDeUsuario());
			mRestaurante.registrarRestaurante(b.getNombreDeUsuario(), callePrincipal, nombre, descripcion,
					imagenInicial, descripcionImagen);
			Notification.info("Restaurante: " + nombre + " registrado correctamente.");
			limpiarCampos();
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			Notification.warning(e.getMessage());
		}
	}

	public String getCallePrincipal() {
		return callePrincipal;
	}

	public void setCallePrincipal(String callePrincipal) {
		this.callePrincipal = callePrincipal;
	}

	public String getNuevaCallePrincipal() {
		return nuevaCallePrincipal;
	}

	public void setNuevaCallePrincipal(String nuevaCallePrincipal) {
		this.nuevaCallePrincipal = nuevaCallePrincipal;
	}

	public String getNuevaCalleSecundaria() {
		return nuevaCalleSecundaria;
	}

	public void setNuevaCalleSecundaria(String nuevaCalleSecundaria) {
		this.nuevaCalleSecundaria = nuevaCalleSecundaria;
	}

	public String getNombre() {
		return nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	public String getDescripcion() {
		return descripcion;
	}

	public void setDescripcion(String descripcion) {
		this.descripcion = descripcion;
	}

	public String getImagenInicial() {
		return imagenInicial;
	}

	public void setImagenInicial(String imagenInicial) {
		this.imagenInicial = imagenInicial;
	}

	public String getDescripcionImagen() {
		return descripcionImagen;
	}
	
	public void setDescripcionImagen(String descripcionImagen) {
		this.descripcionImagen = descripcionImagen;
	}

	public List<Direccion> getDirecciones() {
		return direcciones;
	}

	public void setDirecciones(List<Direccion> direcciones) {
		this.direcciones = direcciones;
	}

	public BeanInicioDeSesion getB() {
		return b;
	}

	public void setB(BeanInicioDeSesion b) {
		this.b = b;
	}
}
