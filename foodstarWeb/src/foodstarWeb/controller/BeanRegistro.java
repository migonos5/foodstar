package foodstarWeb.controller;

import java.io.Serializable;
import java.util.List;

import javax.annotation.PostConstruct;
import javax.ejb.EJB;
import javax.enterprise.context.SessionScoped;
import javax.inject.Named;

import foodstarEJB.model.entities.Estado;
import foodstarEJB.model.entities.TipoDePersona;
import foodstarEJB.model.managers.ManagerPersona;
import foodstarWeb.view.util.Notification;

@Named
@SessionScoped
public class BeanRegistro implements Serializable {
	@EJB
	private ManagerPersona m;
	private String username;
	private String correoElectronico;
	private String contrasenia;
	private String nombre;
	private String apellido;

	public BeanRegistro() {
		// TODO Auto-generated constructor stub
	}

	public void actionListenerRegistrarUsuario() {
		try {
			m.registrarUsuario(username, correoElectronico, contrasenia, nombre, apellido);
			Notification.info("Usuario registrado correctamente");
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			Notification.warning(e.getMessage());
		}
	}

	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	public String getCorreoElectronico() {
		return correoElectronico;
	}

	public void setCorreoElectronico(String correoElectronico) {
		this.correoElectronico = correoElectronico;
	}

	public String getContrasenia() {
		return contrasenia;
	}

	public void setContrasenia(String contrasenia) {
		this.contrasenia = contrasenia;
	}

	public String getNombre() {
		return nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	public String getApellido() {
		return apellido;
	}

	public void setApellido(String apellido) {
		this.apellido = apellido;
	}
}
