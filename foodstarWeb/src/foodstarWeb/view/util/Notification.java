package foodstarWeb.view.util;

import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;

public class Notification {
	public static void info(String mensaje) {
		FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_INFO, mensaje, null));
	}

	public static void warning(String mensaje) {
		FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_WARN, mensaje, null));
	}

	public static void error(String mensaje) {
		FacesContext.getCurrentInstance().addMessage(null,
				new FacesMessage(FacesMessage.SEVERITY_ERROR, mensaje, null));
	}
}
